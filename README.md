
# Istall the project

1. First you need to clone the project in your machine
```shell
mkdir project_name
cd project_name

git clone git@gitlab.com:joekesov/api-platform-based.git .
```

2. Go to ./api directory and create a new .env file and copy in it the content of .env.example file in the same directory.

3. Go to ./docker directory and from .env.example file create a new file with the content. There you could choose
the port for the enginx
```
NGINX_PORT=8085
```

4. While you are in ./docker directory execute the bellow command to build and run the docker containers 
```shell
docker-compose build
docker-compose up -d
```

You could check if the containers were built and are running with the next command:
```shell
docker container ps
```
And if everything is allright you would see something simmilar to:

| CONTAINER ID | IMAGE        | COMMAND                | CREATED        | STATUS       | PORTS                | NAMES          |
| :----------: | :----------: | :--------------------: | :------------: | :----------: | :------------------: | :------------: |
| 470c705c3065 | docker-nginx | "/docker-entrypoint.…" | 10 seconds ago | Up 7 seconds | 0.0.0.0:8085->80/tcp | docker-nginx-1 |
| 9b9918c234b3 | docker-php   | "docker-php-entrypoi…" | 10 seconds ago | Up 7 seconds | 9000/tcp             | docker-php-1   |


5. Now you could enter to the php container and run composer install
```shell
docker-compose exec php bash
```
Probably your prompt will change and you could see something similar to
```shell
root@9b9918c234b3:/var/www/html
```
in this terminall go to api directory and run composer install:
```shell
cd api
composer install
```

6. While you are still in the php container shell you could execute the command. There is some testing
data in data directory

```shell
bin/console process-offers /var/www/html/api/data/original_file.csv csv
bin/console process-offers /var/www/html/api/data/wrong_original_file.csv csv
...
bin/console process-offers /var/www/html/api/data/offers_01.json json
```

- to run all tests
```shell
vendor/bin/phpunit
```
