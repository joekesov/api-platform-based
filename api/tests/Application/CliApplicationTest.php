<?php

namespace Tests\Application;

use App\Command\ProcessOffersCommand;
use App\Service\FileReaderService;
use App\Service\LotService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class CliApplicationTest extends TestCase
{
    private CommandTester $commandTester;

    protected function setUp(): void
    {
        parent::setUp();

        $fileReaderService = new FileReaderService();
        $lotService = new LotService();

        $command = new ProcessOffersCommand($fileReaderService, $lotService);

        $application = new Application();
        $application->add($command);

        $this->commandTester = new CommandTester($command);
    }

    public function testProcessOffersCommand(): void
    {
        $filename = realpath(__DIR__ . '/../data/offers_01.csv');

        if ($filename === false) {
            $this->fail('Test CSV file not found');
        }

        $input = [
            'filename' => $filename,
            'format' => 'csv',
        ];

        $this->commandTester->execute($input);

        // Write assertions for the expected result
        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString('Negotiation 1', $output);
        // Add more assertions based on the expected output of the command
    }
}
