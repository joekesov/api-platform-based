<?php

use App\Service\FileReaderService;
use App\Service\LotService;
use PHPUnit\Framework\TestCase;

class LotIntegrationTest extends TestCase
{
    private FileReaderService $fileReaderService;
    private LotService $lotService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->fileReaderService = new FileReaderService();
        $this->lotService = new LotService();
    }

    public function testCreateLotsFromCsvFile(): void
    {
        $filename = realpath(__DIR__ . '/../data/offers_01.csv');

        if ($filename === false) {
            $this->fail('Test CSV file not found');
        }

        $offers = $this->fileReaderService->readFile($filename, 'csv');
        $lots = $this->lotService->createLots($offers);

        // Write assertions for the expected result
        $this->assertIsArray($lots);
        // Add more assertions based on the expected structure and values of $lots
    }
}