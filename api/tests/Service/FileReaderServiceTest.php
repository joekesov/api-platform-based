<?php

namespace App\Tests\Service;

use App\Service\FileReaderService;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

use App\Model\Offer;
use App\Model\Price;

class FileReaderServiceTest extends TestCase
{
    public function testReadFileWithUnsupportedFormat(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $fileReader = new FileReaderService();
        $fileReader->readFile('/../data/unsupported_format', 'unsupported_format');
    }

    public function testReadFileWithWrongPriceFormat(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $fileReader = new FileReaderService();
        $fileReader->readFile('/../data/wrong_offers.csv', 'csv');
    }

    public function testReadFileWithWrongJsonFormat(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $fileReader = new FileReaderService();
        $fileReader->readFile('/../data/json/wrong_offers.json', 'json');
    }

    public function testReadFileWithNonexistentFile(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $fileReader = new FileReaderService();
        $fileReader->readFile('nonexistent_file.csv', 'csv');
    }

    public function testReadExampleCsvFile()
    {
        $this->testReadExampleFile('/../data/offers_01.csv', 'csv');
    }

    public function testReadExampleJsonFile()
    {
        $this->testReadExampleFile('/../data/json/offers_01.json', 'json');
    }

    private function testReadExampleFile(string $filePath, string $fileType)
    {
        $filePath = sprintf('%s%s', __DIR__, $filePath);

        $fileReaderService = new FileReaderService();

        $offers = $fileReaderService->readFile($filePath, $fileType);

        $expectedOffers = $this->getExpectedOffers();

        foreach ($offers as $offerName => $items) {
            foreach ($items as $itemName => $offer) {
                $expectedPrice = $expectedOffers[$offerName][$itemName];
                $actualPrice = ['value' => $offer->getPrice()->getValue(), 'currency' => $offer->getPrice()->getCurrency()];

                $this->assertEquals($expectedPrice, $actualPrice, "Incorrect price for $itemName in $offerName offer");
            }
        }
    }

    private function getExpectedOffers(): array
    {
        return [
            'Offer A' => [
                '100 tons of steel' => ['value' => 200000, 'currency' => '€'],
                '50 tons of nickel' => ['value' => 400000, 'currency' => '€'],
                '250 screw drivers' => ['value' => 20000, 'currency' => '€'],
                '200 liters of machine oil' => ['value' => 40000, 'currency' => '€']
            ],
            'Offer B' => [
                '100 tons of steel' => ['value' => 190000, 'currency' => '€'],
                '50 tons of nickel' => ['value' => 380000, 'currency' => '€'],
                '250 screw drivers' => ['value' => 15000, 'currency' => '€'],
            ],
            'Offer C' => [
                '100 tons of steel' => ['value' => 195000, 'currency' => '€'],
                '50 tons of nickel' => ['value' => 390000, 'currency' => '€'],
                '200 liters of machine oil' => ['value' => 38000, 'currency' => '€']
            ]
        ];
    }
}
