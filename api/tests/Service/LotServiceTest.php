<?php

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\LotService;
use App\Model\Offer;
use App\Model\Price;
use App\Model\Lot;
use App\Model\LotItem;

class LotServiceTest extends TestCase
{
    public function testCreateLots()
    {
        $offers = [
            'Offer A' => [
                '100 tons of steel' => new Offer('Offer A', new Price(200000, '€')),
                '50 tons of nickel' => new Offer('Offer A', new Price(400000, '€')),
                '250 screw drivers' => new Offer('Offer A', new Price(20000, '€')),
                '200 liters of machine oil' => new Offer('Offer A', new Price(40000, '€')),
            ],
            'Offer B' => [
                '100 tons of steel' => new Offer('Offer B', new Price(190000, '€')),
                '50 tons of nickel' => new Offer('Offer B', new Price(380000, '€')),
                '250 screw drivers' => new Offer('Offer B', new Price(15000, '€')),
            ],
            'Offer C' => [
                '100 tons of steel' => new Offer('Offer C', new Price(195000, '€')),
                '50 tons of nickel' => new Offer('Offer C', new Price(390000, '€')),
                '200 liters of machine oil' => new Offer('Offer C', new Price(38000, '€')),
            ],
        ];

        $lotService = new LotService();
        $lots = $lotService->createLots($offers);

        $this->assertCount(3, $lots);

        $expectedLots = [
            new Lot([
                new LotItem('100 tons of steel', [
                    'Offer A' => new Offer('Offer A', new Price(200000, '€')),
                    'Offer B' => new Offer('Offer B', new Price(190000, '€')),
                    'Offer C' => new Offer('Offer C', new Price(195000, '€')),
                ]),
                new LotItem('50 tons of nickel', [
                    'Offer A' => new Offer('Offer A', new Price(400000, '€')),
                    'Offer B' => new Offer('Offer B', new Price(380000, '€')),
                    'Offer C' => new Offer('Offer C', new Price(390000, '€')),
                ]),
            ]),
            new Lot([
                new LotItem('250 screw drivers', [
                    'Offer A' => new Offer('Offer A', new Price(20000, '€')),
                    'Offer B' => new Offer('Offer B', new Price(15000, '€')),
                ]),
            ]),
            new Lot([
                new LotItem('200 liters of machine oil', [
                    'Offer A' => new Offer('Offer A', new Price(40000, '€')),
                    'Offer C' => new Offer('Offer C', new Price(38000, '€')),
                ]),
            ]),
        ];

        $this->assertEquals($expectedLots, $lots);
    }
}
