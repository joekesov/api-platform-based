<?php

namespace App\Service;

use App\Model\Lot;
use App\Model\LotItem;

class LotService
{
    public function createLots(array $offers): array
    {
        $groups = [];

        // Group items by their availability in the offers
        foreach ($offers as $supplier => $items) {
            foreach ($items as $item => $offer) {
                if ($offer->getPrice()->getValue() !== null) {
                    $groups[$item][] = $supplier;
                }
            }
        }

        // Merge groups with the same suppliers
        $lotGroups = [];
        foreach ($groups as $item => $suppliers) {
            $key = implode(',', $suppliers);
            if (!isset($lotGroups[$key])) {
                $lotGroups[$key] = [];
            }
            $lotGroups[$key][] = $item;
        }

        // Create lots with full information
        $lots = [];
        foreach ($lotGroups as $suppliers => $items) {
            $lotItems = [];
            foreach ($items as $item) {
                $offersForItem = [];
                foreach (explode(',', $suppliers) as $supplier) {
                    $offersForItem[$supplier] = $offers[$supplier][$item];
                }
                $lotItems[] = new LotItem($item, $offersForItem);
            }
            $lots[] = new Lot($lotItems);
        }

        return $lots;
    }
}
