<?php

namespace App\Service;

use InvalidArgumentException;

use App\Model\Offer;
use App\Model\Price;

class FileReaderService
{
    public function readFile(string $filename, string $format): array
    {
        if (!file_exists($filename)) {
            throw new InvalidArgumentException("File not found: $filename");
        }

        $content = file_get_contents($filename);

        switch ($format) {
            case 'csv':
                return $this->parseCsv($content);
            case 'json':
                return $this->parseJson($content);
            default:
                throw new InvalidArgumentException(sprintf("Unsupported format: %s", $format));
        }
    }

    private function parseCsv(string $content): array
    {
        $lines = explode(PHP_EOL, $content);
        $header = str_getcsv(array_shift($lines));

        if (empty($header) || !in_array('Requested Item', $header) || $header[0] !== 'Requested Item') {
            throw new InvalidArgumentException("Invalid header in CSV file");
        }

        $offers = [];
        foreach ($lines as $line) {
            if (trim($line) === '') {
                continue;
            }

            $row = str_getcsv($line);
            $item = array_shift($row);


            foreach ($row as $index => $priceData) {
                $offerName = $header[$index + 1];
                $priceData = $this->parsePriceData($priceData);
                if ($priceData !== null) {
                    $price = new Price($priceData['value'], $priceData['currency']);
                    $offers[$offerName][$item] = new Offer($offerName, $price);
                }
            }
        }

        return $offers;
    }

    private function parseJson(string $content): array
    {
        $json = json_decode($content, true);
        $headers = $json['headers'];
        $data = $json['data'];

        if (empty($headers) || $headers[0] !== 'Requested Item') {
            throw new InvalidArgumentException("Invalid header in JSON data");
        }

        $offers = [];
        foreach ($data as $row) {
            $item = $row[0];
            for ($i = 1; $i < count($row); $i++) {
                $offerName = $headers[$i];
                $priceData = $this->parsePriceData($row[$i]);
                if ($priceData !== null) {
                    $price = new Price($priceData['value'], $priceData['currency']);
                    $offers[$offerName][$item] = new Offer($offerName, $price);
                }
            }
        }

        return $offers;
    }

    private function parsePriceData(string $priceData): ?array
    {
        if (!is_string($priceData)) {
            throw new InvalidArgumentException('Price data must be a string');
        }

        if (trim(strtolower($priceData)) === 'unavailable') {
            return null;
        }

        $pattern = '/(?P<currency>[^0-9\s]+)?\s*(?P<value>[0-9\s]+(\.[0-9]+)?)/';
        if (!preg_match($pattern, $priceData, $matches)) {
            throw new InvalidArgumentException('Invalid price data format');
        }

        return [
            'currency' => $matches['currency'] ?? '',
            'value' => (float)str_replace(' ', '', $matches['value']),
        ];
    }
}

