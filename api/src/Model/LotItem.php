<?php

namespace App\Model;

class LotItem
{
    private string $name;
    private array $offers;

    public function __construct(string $name, array $offers)
    {
        $this->name = $name;
        $this->offers = $offers;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOffers(): array
    {
        return $this->offers;
    }
}