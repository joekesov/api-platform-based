<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use App\Service\FileReaderService;
use App\Service\LotService;

#[AsCommand(
    name: 'process-offers',
    description: 'Add a short description for your command',
)]
class ProcessOffersCommand extends Command
{
    private $lotService;
    private $fileReaderService;

    public function __construct(FileReaderService $fileReaderService, LotService $lotService)
    {
        $this->lotService = $lotService;
        $this->fileReaderService = $fileReaderService;
        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->addArgument('filename', InputArgument::REQUIRED, 'The file name to read the offers from')
            ->addArgument('format', InputArgument::REQUIRED, 'The format of the file (csv or txt)');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $filename = $input->getArgument('filename');
        $format = $input->getArgument('format');

        $offers = $this->fileReaderService->readFile($filename, $format);

        // Perform input validation here, if needed

        $lots = $this->lotService->createLots($offers);
        $this->writeResults($io, $lots);


        return Command::SUCCESS;
    }

    protected function writeResults($io, $lots)
    {
        foreach ($lots as $index => $lot) {
            $io->writeln("Negotiation " . ($index + 1));

            // Initialize table headers and rows
            $headers = ['Requested Item'];
            $rows = [];

            // Prepare table headers and rows
            foreach ($lot->getItems() as $lotItem) {
                $row = [$lotItem->getName()];

                foreach ($lotItem->getOffers() as $supplier => $offer) {
                    // Add supplier to headers if not already added
                    if (!in_array($supplier, $headers)) {
                        $headers[] = $supplier;
                    }

                    // Add price data to the row
                    $formattedPrice = $offer->getPrice()->getCurrency() . ' ' . number_format($offer->getPrice()->getValue(), 0, '.', ' ');
                    $row[] = $formattedPrice;
                }

                $rows[] = $row;
            }

            // Display table
            $io->table($headers, $rows);
        }
    }
}
